from flask import Flask, request, render_template
import subprocess
import toolforge

app = Flask(__name__)


QUERY = """
SELECT ips_item_id
FROM wb_items_per_site
WHERE ips_site_id = %s
AND ips_site_page IN ({});
"""


def get_revision():
    try:
        output = subprocess.check_output(["git", "describe", "--always"], stderr=subprocess.STDOUT).strip().decode()
        assert 'fatal' not in output
        return output
    except Exception:
        # if somehow git version retrieving command failed, just return
        return ''


revision = get_revision()


@app.context_processor
def inject_base_variables():
    return {
        "revision": revision,
    }


@app.route('/', methods=["GET"])
def show_form():
    return render_template('form.html')


@app.route('/', methods=["POST"])
def do_query():
    pages = list(map(lambda s: s.strip(), request.values.get("pages").split("\n")))
    format_strings = ','.join(['%s'] * len(pages))

    full_query = QUERY.format(format_strings)
    params = [request.values.get("wiki")]
    params.extend(pages)
    # print(full_query, params)

    conn = toolforge.connect('wikidatawiki', 'web')
    with conn.cursor() as cur:
        cur.execute(full_query, params)
        raw_results = cur.fetchall()
    conn.close()

    results = "\n".join(list(map(lambda result: "Q" + str(result[0]), raw_results)))
    return render_template('results.html', results=results)


if __name__ == '__main__':
    app.run()
